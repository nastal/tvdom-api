<?php

namespace App\Services;

use App\Models\VOD;

class VideoOnDemandService extends FlussonicCommunicationService
{

    /**
     * @param int $resourceId
     * @return mixed
     */
    public function getVodUrlList(int $resourceId)
    {
        //todo make cached
        $sources = VOD::streamSources($resourceId);

        if (!$sources) {
            abort(
                response()->json(['message' => 'Video resource not found'], 404)
            );
        }

        return $sources->map(function ($item) {
            $item[VOD::PATH] = $this->getFullStreamUrl($item[VOD::PATH]);
            $item[VOD::BY_DEFAULT] = $item[VOD::QUAL_INDEX] == VOD::QTY_LABEL_3 ? true: false;
            return $item;
        });
    }
}
