<?php

namespace App\Services;

use App\Models\StreamServers;
use App\Models\VOD;
use App\Helpers\Crypto;

/*
 * We don't use direct API to connect to Flussonic as we have mysql database w/ Flussonic actual data
 *
 * token=DEJFTDSDPASS for developer purposes or smartTV??
 */

/**
 * Class FlussonicCommunicationService
 * @package App\Services
 */
class FlussonicCommunicationService
{

    const URL_QUERY = '?';
    const URL_EQUALS = '=';
    const URL_TOKEN_PARAM = 'token';
    const HLS_PARAM = '/index.m3u8';

    const VALID_TO_SEC = 300;

    const TOKEN_PARAM_CLIENT_IP = 'ClientIp';
    const TOKEN_PARAM_RESOURCE = 'ResourceId';
    const TOKEN_PARAM_VALIDTO = 'ValidTo';
    const TOKEN_PARAM_PLAYTO = 'PlayTo'; //optional
    const TOKEN_PARAM_DEMO = 'Demo';
    const TOKEN_PARAM_USERID = 'UserId';

    protected $tokenParams = [
        self::TOKEN_PARAM_CLIENT_IP, //* required string IP address
        self::TOKEN_PARAM_RESOURCE, //* required string ex. /orig/PBKLV_20191125-2315_urgant_329360987_orig.mp4
        self::TOKEN_PARAM_VALIDTO, //* required unix timestamp
        self::TOKEN_PARAM_DEMO => false, //default false
        self::TOKEN_PARAM_USERID => null, //default null
    ];

    protected $clientIp;

    protected $streamServer;

    //todo make geo restrictions / flussonic side???

    public function __construct()
    {
        $this->clientIp = env('APP_DEBUG') ? config('flussonic.env_ip') : request()->ip();
        $this->streamServer = $this->getAvailableStreamServer();
    }

    /**
     * @return mixed
     */
    protected function getAvailableStreamServer()
    {
        //todo make cached
        return StreamServers::AvalaibleStreamers();
    }


    /**
     * @param string $resource
     * @return string
     * @throws \App\Helpers\CannotPerformOperationException
     */
    protected function getFullStreamUrl(string $resource)
    {
        $authData = $this->tokenParams;
        $authData[self::TOKEN_PARAM_CLIENT_IP] = $this->clientIp;
        $authData[self::TOKEN_PARAM_RESOURCE] = $resource;
        $authData[self::TOKEN_PARAM_VALIDTO] = time() + self::VALID_TO_SEC;

        $url = $this->streamServer . $resource . self::HLS_PARAM;

        return $url . self::URL_QUERY . self::URL_TOKEN_PARAM . self::URL_EQUALS . $this->encryptToken($authData);
    }

    /**
     * @param array $payload
     * @return string
     * @throws \App\Helpers\CannotPerformOperationException
     */
    protected function encryptToken(array $payload)
    {
        $cipherText = Crypto::Encrypt(serialize($payload), config('flussonic.crypto_key'));
        return strtr(base64_encode($cipherText), '+/=', '-_,');
    }

}
