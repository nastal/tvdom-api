<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\VodArchive;
use App\Models\VodImage;
use App\Models\BmaArchive;
use Illuminate\Support\Facades\Log;
use Carbon\Carbon;

class GrabLatestVod extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'grab:vod';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Grab latest VOD entries';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $lastId = VodArchive::max('bma_archive_id') ?: (int)env('VOD_INIT_ID');

        $upcomingVod = BmaArchive::withDescription()->where('bma_archive.id', '>', $lastId);

        foreach ($upcomingVod->cursor() as $VodItem) {
            $imagePath = $VodItem->series_image && !empty($VodItem->series_image) ? $VodItem->series_image : VodImage::DEF_IMG;
            $image = VodImage::firstOrCreate(['path' => $imagePath]);

            //skip youTubes and other stuff without stream ID
            if (!$VodItem->remote_record_id) {
                continue;
            }

            VodArchive::firstOrCreate(
                [
                    'title'          => $VodItem->default_title,
                    'bma_archive_id' => $VodItem->id,
                    'stream_id'      => $VodItem->remote_record_id,
                    'description'    => $VodItem->archive_description,
                    'image_id'       => $image->id,
                    'publish_date'   => Carbon::parse($VodItem->date_time_added),
                ]
            );
        }
    }
}
