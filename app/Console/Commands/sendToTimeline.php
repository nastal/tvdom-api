<?php

namespace App\Console\Commands;

use App\Jobs\ProcessVodToTimeLine;
use Illuminate\Console\Command;
use App\Models\VodArchive;

class sendToTimeline extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'tmljob:send';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send to ProcessVodToTimeLine Job';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //send All non processed and create a job to each one
        $nonSendVod = VodArchive::where('job_done', false);

        foreach ($nonSendVod->cursor() as $VodItem) {
            ProcessVodToTimeLine::dispatch($VodItem);
        }
    }
}
