<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;
use Illuminate\Routing\UrlGenerator;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(
            App\Services\FlussonicCommunicationService::class,
            App\Services\VideoOnDemandService::class
        );
    }


    /**
     * Bootstrap any application services.
     * @param UrlGenerator $url
     */
    public function boot(UrlGenerator $url)
    {
        if (env('REDIRECT_HTTPS')) {
            $url->forceScheme('https');
        }
        Schema::defaultStringLength(191);
    }
}
