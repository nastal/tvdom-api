<?php

namespace App\Jobs;

use App\Models\VodArchive;
use App\Models\VodImage;
use Carbon\Carbon;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Log;
use GuzzleHttp\Client;

class ProcessVodToTimeLine implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $vod;

    protected $params;

    const POST = 'POST';
    const VIDEO = 'video';
    const IMAGE = 'image';

    const SMOTR_CAT = 18;
    const NODE_ADD_IMAGE = 'node/add_image/';

    public function __construct(VodArchive $vod)
    {
        $this->vod = $vod;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {

        try {
            $client = new Client();
            $response = $client->request(
                self::POST,
                config('services.timeline.url') . self::VIDEO,
                ['headers' =>
                     [
                         'Authorization' => 'Bearer ' . config('services.timeline.token')
                     ],
                 'form_params' =>
                     [
                         'media_id' => $this->vod->stream_id,
                         'title'    => $this->vod->title,
                         'body'     => $this->vod->description,
                         'duration' => '0', //todo when acceseble from flussonic
                         'is_active' => 1, //todo ??
                         'published_at' => Carbon::parse($this->vod->publish_date)->toDateTimeString(),
                         'main_category_id' => self::SMOTR_CAT,
                     ]
                ]
            );
            $this->vod->job_done = true;
            $this->vod->save();
        } catch (\Exception $exception) {
            $this->fail($exception);
            return false;
        }

        $node = (string) $response->getBody();
        $node = json_decode($node);

        $node_id = $node->node_id;

        $imageId = $this->vod->image_id;
        $path = VodImage::find($imageId);
        $path = VodImage::TVDOM_HTTP . $path->path;

        if (@getimagesize($path)) {
            $client = new Client();
            $response = $client->request('POST', config('services.timeline.url') . self::IMAGE, [
                'headers' =>
                    [
                        'Authorization' => 'Bearer ' . config('services.timeline.token')
                    ],
                'multipart' => [
                    [
                        'name'     => 'created_at',
                        'contents' => Carbon::parse($this->vod->publish_date)->toDateTimeString()
                    ],
                    [
                        'name'     => 'image',
                        'contents' => fopen($path, 'r')
                    ],
                    [
                        'name'     => 'description',
                        'contents' => 'Скриншот из видео'
                    ]
                ]
            ]);

            $image = (string) $response->getBody();
            $image = json_decode($image);


            $client->request('POST', config('services.timeline.url') . self::NODE_ADD_IMAGE . $node_id, [
                'headers' =>
                    [
                        'Authorization' => 'Bearer ' . config('services.timeline.token')
                    ],
                'form_params' => [
                    'node_id' => $node_id,
                    'image_id' => $image->id,
                    'is_visible' => 1,
                    'is_default' => 1
                ]
            ]);
        }
    }

    /**
     * @param null $exception
     */
    public function fail($exception = null)
    {
        Log::error($exception->getMessage());
    }
}
