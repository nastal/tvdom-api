<?php


namespace App\Helpers;

class JwPlayerHelper
{

    const NOT_FOUND = 'Resource not found';

    public static function getVastLink($resourceId)
    {
        return str_replace(
            '[placeholder]',
            urlencode(config('jwplayer.iframe_tvdom_link') . $resourceId),
            config('jwplayer.vast_link')
        );
    }

    public static function getIframe($resourceId)
    {
        $view = view('iframe', ['resourceId' => $resourceId])->render();
        return trim(preg_replace('/\r\n/', ' ', $view));
    }
}
