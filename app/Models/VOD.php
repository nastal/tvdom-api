<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Traits\ReadOnlyTrait;

class VOD extends Model
{

    const QUALITY = 'kvalitate_';
    const READY = '_pieejama';
    const READY_INT = 1;
    const ITER_OFFSET = 1;
    const QTY_LABEL = 'QTY_LABEL_';
    const QTY_LABEL_1 = '160';
    const QTY_LABEL_2 = '320';
    const QTY_LABEL_3 = 'oirg';
    const BY_DEFAULT = 'default';

    const PATH = 'file';
    const QUAL_INDEX = 'label';

    const QUAL_COLUMNS = [
        [
            self::QUALITY . 1,
            self::QUALITY . 1 . self::READY
        ],
        [
            self::QUALITY . 2,
            self::QUALITY . 2 . self::READY,
        ],
        [
            self::QUALITY . 3,
            self::QUALITY . 3 . self::READY,
        ]
    ];

    use ReadOnlyTrait;

    /**
     * @var string
     */
    protected $connection = 'mysql_streamer';

    /**
     * @var string
     */
    protected $table = 'SATURS_B';

    /**
     * @var string
     */
    protected $primaryKey = 'uid';

    /**
     * @param $query
     * @param $resourceId
     * @return \Illuminate\Support\Collection
     */
    public function scopeStreamSources($query, $resourceId)
    {
        $sources = $query->select($this->getStreamSourcesColumns())->where('satura_id', $resourceId);
        $vodSources = $sources->first();
        return !$vodSources ? false : $this->filterAvailableSources($vodSources);
    }

    protected function getStreamSourcesColumns()
    {
        return collect(self::QUAL_COLUMNS)->flatten()->toArray();
    }

    /**
     * @param $sources
     * @return \Illuminate\Support\Collection
     */
    public function filterAvailableSources($sources)
    {
        $splittedSources = collect($sources->toArray())->split(count(self::QUAL_COLUMNS));
        $paths = collect();

        for ($i = 0; $i < count(self::QUAL_COLUMNS); $i++) {
            if ($this->isActiveQuality($splittedSources[$i])) {
                $qualityLabel = self::QTY_LABEL . ($i + self::ITER_OFFSET);
                $paths->push([
                    self::PATH => $splittedSources[$i]->toArray()[self::QUALITY . ($i + self::ITER_OFFSET)],
                    self::QUAL_INDEX => constant("self::$qualityLabel")
                ]);
            }
        }

        return $paths->reverse()->values();
    }

    /**
     * @param $source
     * @return bool
     */
    protected function isActiveQuality($source)
    {
        return $source->each(function ($item, $key) {
            if (strpos($key, self::READY) !== false) {
                return $item === self::READY_INT;
            }
        });
    }
}
