<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class VodArchive extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['title', 'bma_archive_id', 'stream_id', 'image_id', 'description', 'publish_date', 'job_done'];

    /**
     * @var string
     */
    protected $table = 'vod_archive';

    public function image()
    {
        return $this->hasOne(VodImage::class, 'id', 'image_id');
    }
}
