<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Traits\ReadOnlyTrait;

class BmaArchive extends Model
{
    use ReadOnlyTrait;

    const RELATED_DESCR = 'bma_channels_description';

    /**
     * @var string
     */
    protected $connection = 'mysql_old';

    /**
     * @var string
     */
    protected $table = 'bma_archive';

    public function scopeWithDescription($query)
    {
        return $query->select(
            'bma_archive.id',
            'bma_archive.default_title',
            'bma_archive.image',
            'bma_archive.series_image',
            'bma_archive.remote_record_id',
            'bma_archive.date_time_added',
            'descr.archive_description'
        )
            ->leftjoin(self::RELATED_DESCR . ' as descr', function ($join) {
                $join->on('descr.archive_id', '=', $this->table . '.id')
                    ->where('descr.language', '=', 'ru');
            });
    }
}
