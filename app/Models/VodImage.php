<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class VodImage extends Model
{

    const DEF_IMG = '/default_big_n.jpg';
    const TVDOM_HTTP = 'https://tvdom.tv';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['path'];

    /**
     * @var string
     */
    protected $table = 'vod_image';

    /**
     * Get the full image path
     *
     * @return string
     */
    public function getFullPathAttribute()
    {
        return self::TVDOM_HTTP . $this->path;
    }

}
