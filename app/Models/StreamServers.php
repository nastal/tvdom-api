<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Traits\ReadOnlyTrait;

class StreamServers extends Model
{

    use ReadOnlyTrait;

    const URL = 'serveris';
    const ENABLED = 'serveris_pieejams';
    const WEIGHT = 'serveris_svars';

    /**
     * @var string
     */
    protected $connection = 'mysql_streamer';

    /**
     * @var string
     */
    protected $table = 'STRAUMES';

    /**
     * @var string
     */
    protected $primaryKey = 'uid';

    /**
     * @param $query
     * @return mixed
     */
    public function scopeAvalaibleStreamers($query)
    {
        return $query->select([StreamServers::URL])
            ->where(StreamServers::ENABLED, 1)
            ->orderBy(StreamServers::WEIGHT, 'desc')
            ->first()
            ->{StreamServers::URL};
    }
}
