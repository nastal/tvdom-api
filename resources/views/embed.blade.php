<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>TVDOM</title>
        <script type="text/javascript" src="{{ URL::asset('js/jwplayer-8.11.7/jwplayer.js') }}"></script>
        <script>jwplayer.key='{{ config('jwplayer.key') }}'</script>
    </head>

    <style>
        body {
            margin: 0;
        }
    </style>

    <body>
        <div id="video"></div>

        <script>

            //todo stream sources switch ??
            //todo cross origin iframe https://voutzinos.com/blog/cross-origin-iframe-laravel/

            var vastLink = '{!! JwPlayerHelper::getVastLink($resourceId) !!}';

            var gemiusObj = @json(config('jwplayer.options.gemius'));
            gemiusObj.materialIdentifier = {{ $resourceId }}

            jwplayer.defaults = @json(config('jwplayer.options.defaults'));
            jwplayer.defaults.sharing.code = '{!! JwPlayerHelper::getIframe($resourceId) !!}';

            for (var prop in jwplayer.defaults.advertising.schedule) {
                jwplayer.defaults.advertising.schedule[prop].tag = vastLink;
                jwplayer.defaults.advertising.schedule[prop].skipoffset = {{ config('jwplayer.skip_offset') }};
            }

            jwplayer('video').setup({
                playlist: [{
                    image: '{{ $image ?? '' }}',
                    sources: @json($videoFile)
                }],
                intl: @json($playerLocale),
                gemiusOptions: gemiusObj,
                abouttext: '{{ config('jwplayer.options.abouttext') }}',
                aboutlink: '{{ config('jwplayer.options.aboutlink') }}',
            });

        </script>
    </body>
</html>
