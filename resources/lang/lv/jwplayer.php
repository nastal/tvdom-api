<?php

return [
    'advertising'   => [
        'admessage'      => 'Reklāma beigsies pēc xx',
        'cuetext'        => 'Reklāma',
        'loadingAd'      => 'Ielādē reklāmu',
        'podmessage'     => 'Reklāma __AD_POD_CURRENT__ no __AD_POD_LENGTH__. ',
        'skipmessage'    => 'Izlaist reklāmu varēsiet pēc xx',
        'skiptext'       => 'Izlaist reklāmu',
        'displayHeading' => 'Reklāma'
    ],
    'airplay'       => 'AirPlay',
    'audioTracks'   => 'Audio celiņi',
    'auto'          => 'Auto',
    'buffer'        => 'Ielādē',
    'cast'          => 'Chromecast',
    'cc'            => 'Subtitri',
    'close'         => 'Aizvērt',
    'errors'        => [
        'badConnection'     => 'Video nevar atskaņot, iespējama problēma ar Jūsu interneta pieslēgumu.',
        'cantLoadPlayer'    => 'Atvainojiet, video atskaņotāju neizdevās ielādēt.',
        'cantPlayInBrowser' => 'Atvainojiet, šajā pārlūkā video nevar atskaņot.',
        'cantPlayVideo'     => 'Šo video nevar atskaņot.',
        'errorCode'         => 'Kļūdas kods',
        'liveStreamDown'    => 'Tiešraide nedarbojas vai ir beigusies.',
        'protectedContent'  => 'Radās problēma iegūt pieeju aizsargātam saturam.',
        'technicalError'    => 'Video nevar atskaņot tehniskas kļūdas dēļ.'
    ],
    'fullscreen'    => 'Pilnekrāna režīms',
    'hd'            => 'Kvalitāte',
    'liveBroadcast' => 'Tiešraide',
    'logo'          => 'Logo',
    'next'          => 'Nākamais',
    'nextUp'        => 'Nākamais',
    'notLive'       => 'Nav tiešraidē',
    'off'           => 'Izslēgt',
    'pause'         => 'Apstādināt',
    'play'          => 'Atskaņot',
    'playback'      => 'Atskaņot',
    'playbackRates' => 'Atskaņošanas ātrums',
    'player'        => 'Video atskaņotājs',
    'playlist'      => 'Atskaņošanas saraksts',
    'poweredBy'     => 'Atskaņots izmantojot',
    'prev'          => 'Iepriekšējais',
    'related'       => [
        'autoplaymessage' => 'Nākamais video sāksies pēc xx',
        'heading'         => 'Skatīt citus video'
    ],
    'replay'        => 'Atkārtot',
    'rewind'        => 'Attīt atpakaļ 10 sekundes',
    'settings'      => 'Iestatījumi',
    'sharing'       => [
        'copied'  => 'Nokopēts',
        'email'   => 'E-Pasts',
        'embed'   => 'Iegult',
        'heading' => 'Kopīgot',
        'link'    => 'Saite'
    ],
    'slider'        => 'Stāvokļa slīdnis',
    'stop'          => 'Apstādināt',
    'videoInfo'     => 'Informācija par video',
    'volume'        => 'Skaļums',
    'volumeSlider'  => 'Skaļuma slīdnis'
];
