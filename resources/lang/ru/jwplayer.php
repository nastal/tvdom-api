<?php

return [
    'advertising'   => [
        'admessage'      => 'Реклама закончится через xx',
        'cuetext'        => 'Реклама',
        'loadingAd'      => 'Загрузка рекламы',
        'podmessage'     => 'Реклама __AD_POD_CURRENT__ no __AD_POD_LENGTH__. ',
        'skipmessage'    => 'Пропустить рекламу через xx',
        'skiptext'       => 'Пропустить рекламу',
        'displayHeading' => 'Реклама'
    ],
    'airplay'       => 'AirPlay',
    'audioTracks'   => 'Аудио дорожка',
    'auto'          => 'Авто',
    'buffer'        => 'Загрузка',
    'cast'          => 'Chromecast',
    'cc'            => 'Субтитры',
    'close'         => 'Закрыть',
    'errors'        => [
        'badConnection'     => 'Видео не производится, проблема а интернет.',
        'cantLoadPlayer'    => 'Не удалось загрузить видео плейер.',
        'cantPlayInBrowser' => 'Нет поддержки браузера.',
        'cantPlayVideo'     => 'Видео не проигривается.',
        'errorCode'         => 'Код ошибки',
        'liveStreamDown'    => 'Трансляция кончилась или не работает.',
        'protectedContent'  => 'Ошибка доступа к защищенному содержанию.',
        'technicalError'    => 'Видео не производится по техническим причинам.'
    ],
    'fullscreen'    => 'Полноекранный режим',
    'hd'            => 'Качество',
    'liveBroadcast' => 'Прямая транслация',
    'logo'          => 'Лого',
    'next'          => 'Следующий',
    'nextUp'        => 'Следующий',
    'notLive'       => 'Нет прямой трансляции',
    'off'           => 'Выключить',
    'pause'         => 'Остановить',
    'play'          => 'Воспроизвести',
    'playback'      => 'Воспроизвести',
    'playbackRates' => 'Скорость',
    'player'        => 'Видео плеер',
    'playlist'      => 'Плейлист',
    'poweredBy'     => 'Воспроизвести используя',
    'prev'          => 'Предыдущий',
    'related'       => [
        'autoplaymessage' => 'Следующий видео начнется через xx',
        'heading'         => 'Смотреть другие видео'
    ],
    'replay'        => 'Повторить',
    'rewind'        => 'Назад 10с',
    'settings'      => 'Установки',
    'sharing'       => [
        'copied'  => 'Скопирован',
        'email'   => 'е-мейл',
        'embed'   => 'встраивать',
        'heading' => 'делиться',
        'link'    => 'ссылка'
    ],
    'slider'        => 'Слайдер',
    'stop'          => 'Остановить',
    'videoInfo'     => 'Информация о видео',
    'volume'        => 'Звук',
    'volumeSlider'  => 'Регулятор громкости'
];
