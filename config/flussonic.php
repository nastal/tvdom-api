<?php


return [

    /*
    |--------------------------------------------------------------------------
    | Crypto Key
    |--------------------------------------------------------------------------
    |
    | Use for symmetrically encrypted token with flussonic server
    |
    */

    'crypto_key' => '..kR0..a+&N.V...',

    /*
    |--------------------------------------------------------------------------
    | Environment IP
    |--------------------------------------------------------------------------
    |
    | Because on some local environments you can't pass client IP to docker
    | without complicated configuration.
    |
    */

    'env_ip' => env('DEV_IP', '80.232.172.44'),

];
