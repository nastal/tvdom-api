<?php

return [

    'key' => env('JWPLAYER_KEY'),

    'options'           => [
        'defaults' => [
            'aspectratio'          => '16:9',
            'autostart'            => true,
            'responsive'           => false,
            'controls'             => true,
            'displaydescription'   => true,
            'displaytitle'         => true,
            'flashplayer'          => '//ssl.p.jwpcdn.com/player/v/8.0.10/jwplayer.flash.swf',
            'mute'                 => false,
            'playbackRateControls' => false,
            'preload'              => 'metadata',
            'width'                => '100%',
            'height'               => '100%',
            'repeat'               => false,
            'stagevideo'           => false,
            'primary'              => 'html5',
            'debug'                => false,
            'cast'                 => [],
            'sharing' => [
                'sites' => ['facebook'],
            ],

            //ADS
            'advertising'          => [
                'client'   => 'vast',
                'schedule' => [
                    'adbreak1' => [
                        'offset'     => 'pre',
                        'tag'        => '',
                        'skipoffset' => ''
                    ],
                    'adbreak2' => [
                        'offset'     => 1200,
                        'tag'        => '',
                        'skipoffset' => ''
                    ],
                    'adbreak3' => [
                        'offset'     => 'post',
                        'tag'        => '',
                        'skipoffset' => ''
                    ]
                ]
            ]
        ],

        'gemius'    => [
            'IDENTIFIER'  => 'zUBFZqNIoQ4GCYTQGWjRVXXn7O81BcR0IrvSCzgrD_v.k7',
            'playerId'    => 'player_archive',
            'programName' => 'PBK'
        ],
        'abouttext' => 'Video Available at www.tvdom.tv',
        'aboutlink' => 'http://www.tvdom.tv',

    ],

    //todo warning. old api
    'iframe_tvdom_link' => 'https://tvdom.tv/play_arhivs/pbk-latvia/',
    'vast_link'         => 'https://pubads.g.doubleclick.net/gampad/ads?correlator=[placeholder]&iu=/21850858195/Preroll_video_tvdom.tv&env=vp&gdfp_req=1&output=vast&sz=640x480&description_url=[placeholder]&tfcd=0&npa=0&vpmute=0&vpa=0&vad_format=linear&vpos=preroll&unviewed_position_start=1',
    'skip_offset'       => 15
];
