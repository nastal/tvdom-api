<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use App\Models\VodArchive;

Route::get('/', function () {

    return view('welcome');
});
Route::get('/embed/{id}', function ($id) {
        App::setLocale('ru'); //todo consumer only rus.timeline.lv change when need
        return view('embed', [
            'videoFile' => app('VODservice')->getVodUrlList($id),
            'resourceId' => $id,
            'image' => VodArchive::where('stream_id', $id)->first()->image->fullPath,
            'playerLocale' => \Lang::get('jwplayer') //todo implement lang
        ]);
});
